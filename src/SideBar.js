import React, {Component} from "react";
import "./SideBar.css";
import DraggableFlatButton from "./dnd/DragSource/DraggableFlatButton";
import SideBarDialog from "./SideBarDialog";
import {Dialog, TextField, FlatButton, List, ListItem} from "material-ui";

class SideBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            specifyNewComponent: undefined,
            nameNewAutomation: undefined
        };
    }

    handleChange(event, index, value) {
        let specifyNewComponent = this.state.specifyNewComponent;
        specifyNewComponent.value = value;
        this.setState({specifyNewComponent: specifyNewComponent});
    }

    render() {
        return (
            <div className="component-condition-trigger-sidebar">
                <h3>Components</h3>
                <div className="components">
                    <List>
                        {this.props.allValidComponents ? this.props.allValidComponents.map(
                            (component, i) => {
                                return <DraggableFlatButton key={"flatButtom-" + i}
                                                            entity_id={component.entity_id}
                                                            label={component.attributes.friendly_name}
                                                            addItem={this.addItem.bind(this)}
                                />;
                            }
                        ) : undefined}
                        <DraggableFlatButton key={"time-flat-button"}
                                             entity_id={"time-constant-button"}
                                             label={"Time"}
                                             addItem={this.addItem.bind(this)}

                        />
                    </List>
                </div>
                <h3>Commands</h3>
                <List>
                    <ListItem primaryText={"Add new automation"} onClick={this.addNewAutomation.bind(this)} />
                    <ListItem primaryText={"Save all automations"} onClick={this.props.saveAndClose} />
                </List>
                <Dialog title="Name new automation"
                        open={this.state.nameNewAutomation !== undefined}
                        actions={[
                            <FlatButton label="Cancel"
                                        primary={true}
                                        onClick={() => {
                                            this.setState({
                                                nameNewAutomation: undefined
                                            });
                                        }} />,
                            <FlatButton label="Save"
                                        primary={true}
                                        onClick={() => {
                                            this.props.addNewAutomation(this.state.nameNewAutomation);
                                            this.setState({
                                                nameNewAutomation: undefined
                                            });
                                        }} />
                        ]}
                >
                    <TextField onChange={(event, value) => {
                        this.setState({
                            nameNewAutomation: value
                        });
                    }} />
                </Dialog>
                <SideBarDialog specifyNewComponent={this.state.specifyNewComponent}
                               services={this.props.services}
                               handleChange={this.handleChange.bind(this)}
                               stateApi={this.props.allValidComponents}
                               cancel={() => this.setState({specifyNewComponent: undefined})}
                               verifyAndSave={this.verifyAndSave.bind(this)} />
            </div>
        );
    }

    addNewAutomation() {
        this.setState({
            nameNewAutomation: ""
        });
    }

    addItem(entity_id, type, automationNumber) {
        this.setState({
            specifyNewComponent: {
                entity_id: entity_id,
                type: type,
                value: entity_id === "time-constant-button" ? 5 : 1,
                automationNumber: automationNumber
            }
        });
    }

    verifyAndSave(newComponent) {
        let type = this.state.specifyNewComponent.type;
        this.props.addItem(newComponent, type, this.props.currentAutomationViewed);
        this.setState({
            specifyNewComponent: undefined
        });
    }
}

export default SideBar;
