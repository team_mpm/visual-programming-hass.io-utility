import React, {Component} from "react";
import SelfConfiguringAutomationComponent from "./components/SelfConfiguringAutomationComponent";
import {Tabs, Tab} from 'material-ui/Tabs';
import {RaisedButton} from "material-ui";

class DragAndDropView extends Component {

    render() {
        if (!this.props.stateApi) return <div />;

        return <Tabs onChange={(a, b, tab) => {
            this.props.changeCurrentAutomationViewed(tab.props.index);
        }}>
            {this.props.automations.map(
                (automation, i) => {
                    return <Tab key={"tab-" + i} label={automation.alias ? automation.alias : automation.id}>
                        <SelfConfiguringAutomationComponent
                            key={"automation-" + i}
                            automation={automation}
                            automationNumber={i}
                            delete={(type, position) => this.props.delete(type, position, i)}
                            services={this.props.services}
                            addItem={this.props.addItem}
                            storeInParent={this.props.storeInParent}
                            customGroupings={this.props.customGroupings}
                            saveCustomGrouping={this.props.saveCustomGrouping}
                            allValidComponents={this.props.allValidComponents}
                            stateApi={this.props.stateApi} />
                        <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderTop: '2px dotted black', marginTop: '10px', paddingTop: '10px', marginLeft: '10px', marginRight: '10px'}}>
                            <RaisedButton primary={true} label={"Groupify"} onClick={() => {
                                this.props.addNewGroups(i);
                            }} />
                            <RaisedButton secondary={true} label={"Delete"} onClick={() => {
                                this.props.deleteAutomation(i);
                            }} />
                        </div>
                    </Tab>;
                })
            }
        </Tabs>;
    }
}

export default DragAndDropView;
