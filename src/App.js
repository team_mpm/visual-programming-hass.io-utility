import React, {Component} from "react";
import "./App.css";
import SideBar from "./SideBar";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import {DragDropContextProvider} from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import DragAndDropView from "./DragAndDropView";
import {
    filterNonComponents, getServicesAsList, loadConfiguration, loadCustomGroupings, loadServices, loadStateApi,
    sendToCompile,
    storeUnusedParametersSeparate
} from "./IOUtility";
import Grouping from "./components/automations/Grouping";
import {addNewRoomToGrouping} from "./Utilities";

class App extends Component {

    constructor(props) {
        super(props);
        loadConfiguration(this.callBackConfiguration);
        loadStateApi(this.callBackStateApi);
        loadServices(this.callBackServices);
        loadCustomGroupings(this.callBackCustomGroupings);
        this.state = {
            stateApi: undefined,
            allValidComponents: undefined,
            configurationWithoutAutomation: undefined,
            automations: undefined,
            services: undefined,
            currentAutomationViewed: 0,
            addGroups: undefined,
            customGroupings: undefined,
            hasMergedGroupingsAndAutomations: false
        };
    }

    render() {
        if (!this.state.configurationWithoutAutomation || !this.state.automations || !this.state.hasMergedGroupingsAndAutomations) {
            return <div />;
        }

        return (
            <DragDropContextProvider backend={HTML5Backend}>
                <MuiThemeProvider>

                    <div className="main-view">
                        <header className="main-view-header">
                            Visual programming hass.io utility
                        </header>
                        <SideBar allValidComponents={this.state.allValidComponents}
                                 services={this.state.services}
                                 addItem={this.addItem}
                                 saveAndClose={this.saveAndClose.bind(this)}
                                 currentAutomationViewed={this.state.currentAutomationViewed}
                                 addNewAutomation={this.addNewAutomation.bind(this)}
                        />
                        <div className="non-sidebar-view">
                            <DragAndDropView addItem={this.addItem}
                                             services={this.state.services}
                                             stateApi={this.state.allValidComponents}
                                             storeInParent={this.handleUpdate.bind(this)}
                                             changeCurrentAutomationViewed={(newAutomation) => {
                                                 this.setState({currentAutomationViewed: newAutomation});
                                             }}
                                             delete={this.deleteComponent.bind(this)}
                                             automations={this.state.automations}
                                             addNewGroups={this.addNewGroups.bind(this)}
                                             customGroupings={this.state.customGroupings ? this.state.customGroupings : {}}
                                             allValidComponents={this.state.allValidComponents}
                                             saveCustomGrouping={(customGroupings) => {
                                                 this.saveNewCustomGrouping(customGroupings);
                                             }}
                                             deleteAutomation={this.deleteAutomation.bind(this)}
                            />
                            <Grouping addGroups={this.state.addGroups}
                                      addRoom={(key, position) => {
                                          let addGroups = this.state.addGroups;
                                          addGroups = addNewRoomToGrouping(addGroups, key, position);
                                          this.setState({
                                              addGroups: addGroups
                                          });
                                      }}
                                      allValidComponents={this.state.allValidComponents}
                                      customGroupings={this.state.customGroupings ? this.state.customGroupings : {}}
                                      save={(customGroupings) => {
                                          this.saveNewCustomGrouping(customGroupings);
                                      }}
                                      cancel={() => {
                                          this.setState({
                                              addGroups: undefined
                                          });
                                      }}
                                      removeGroup={this.removeGroup.bind(this)}
                            />
                        </div>
                    </div>
                </MuiThemeProvider>
            </DragDropContextProvider>
        );
    }

    addNewGroups(automationNumber) {
        // search for automation in state
        let automation = this.state.automations[automationNumber];
        // Fetch every field with entity id (and it's context)
        let fields = {};
        fields.trigger = this.mapComponentToEntityIds(automation.trigger, "trigger", this.state.customGroupings[automation.alias]);
        fields.condition = this.mapComponentToEntityIds(automation.condition, "condition", this.state.customGroupings[automation.alias]);
        fields.action = this.mapComponentToEntityIds(automation.action, "action", this.state.customGroupings[automation.alias]);
        fields.automation = automation.alias;

        // Save this in state as an object with key=context/label and value = object with fields for every office
        this.setState({
            addGroups: fields
        });
    }

    mapComponentToEntityIds(component, componentType, outerMatchingCustomGrouping) {
        let returnArr = [];

        component.forEach(
            (element) => {
                let additionDescription = "";
                switch (componentType) {
                    case "trigger":
                        additionDescription = element.platform;
                        break;
                    case "condition":
                        additionDescription = element.condition;
                        break;
                    case "action":
                        additionDescription = element.service;
                        break;
                    default:
                        break;
                }

                let entity_id = componentType === "action" ? element.data ? element.data.entity_id : undefined : element.entity_id;
                if (entity_id || entity_id === "") {
                    let entity_ids = {};
                    if (outerMatchingCustomGrouping && outerMatchingCustomGrouping[componentType]) {
                        let matchingCustomGrouping = outerMatchingCustomGrouping[componentType];
                        matchingCustomGrouping.forEach(
                            (grouping) => {
                                if (grouping.entity_ids.original_office === entity_id) {
                                    entity_ids = grouping.entity_ids;
                                }
                            }
                        );
                    }
                    let obj = {
                        label: additionDescription,
                        entity_ids: entity_ids
                    };
                    returnArr.push(obj);
                }
            }
        );
        return returnArr;
    }

    addNewAutomation(automationName) {
        // Check that name doesn't exist. If it doesnt, add new
        if (this.state.automations.find(
                (automation) => {
                    return automation.alias.toLowerCase() === automationName.toLowerCase();
                }
            ) === undefined) {
            let newObject = {
                trigger: [],
                condition: [],
                action: [],
                alias: automationName,
                id: this.generateGuid()
            };
            // No automation with this name exist

            let automations = this.state.automations;
            automations.push(newObject);
            this.setState({
                automations: automations
            });


        } else {
            // todo report that automation name already exist
        }
    }

    generateGuid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4();
    }

    handleUpdate(object, type, automationNumber, positionInAutomation) {
        let automations = this.state.automations;
        if (type === "trigger") {
            automations[automationNumber].trigger[positionInAutomation] = object;
        }
        else if (type === "condition") {
            automations[automationNumber].condition[positionInAutomation] = object;
        }
        else if (type === "action") {
            automations[automationNumber].action[positionInAutomation] = object;
        }

        this.setState({
            automations: automations
        });
    }

    callBackStateApi = (result) => {
        let components = filterNonComponents(result);
        this.setState({
            stateApi: result,
            allValidComponents: components
        });
    };
    callBackConfiguration = (result) => {
        let arr = storeUnusedParametersSeparate(result);
        this.setState({
            configurationWithoutAutomation: arr[0],
            automations: arr[1]
        });
        this.mergeGroupingsAndAutomations(undefined, arr[1]);
    };
    callBackServices = (result) => {
        let servicesAsList = getServicesAsList(result);
        this.setState({
            services: servicesAsList
        });
    };

    callBackCustomGroupings = (resultAsJson) => {
        this.setState({
            customGroupings: resultAsJson
        });
        this.mergeGroupingsAndAutomations(resultAsJson);
    };

    addItem = (newObject, type, automationNumber) => {
        let automations = this.state.automations;
        if (type === "trigger") {
            automations[automationNumber].trigger.push(newObject);
        }
        else if (type === "condition") {
            automations[automationNumber].condition.push(newObject);
        }
        else if (type === "action") {
            automations[automationNumber].action.push(newObject);
        }

        this.setState({
            automations: automations
        });
    };

    saveAndClose() {

        let returnConfiguration = {};
        returnConfiguration["configuration.yaml"] = this.state.configurationWithoutAutomation;
        let automations = this.state.automations;
        automations = this.splitBasedOnCustomGroupings(automations, this.state.customGroupings);

        returnConfiguration["automations.yaml"] = automations;
        let asJson = JSON.stringify(returnConfiguration);
        sendToCompile(asJson, this.state.customGroupings);
    }

    overWriteFirstEntityId(currentAutomation, customGroupingsValue, type, platformName) {
        customGroupingsValue[type].forEach(
            (currentGrouping) => {
                let label = currentGrouping.label;
                let entity_ids = currentGrouping.entity_ids;
                for (let i = 0; i < currentAutomation[type].length; i++) {
                    if (currentAutomation[type][i][platformName] === label) {
                        if (type === "action") {
                            currentAutomation[type][i].data.entity_id = entity_ids.original_office;
                        } else {
                            currentAutomation[type][i].entity_id = entity_ids.original_office;
                        }
                    }
                }
            }
        );

        return currentAutomation;
    }

    saveNewCustomGrouping(customGroupings) {
        let automations = this.state.automations;
        Object.keys(customGroupings).forEach(
            (customGroupingsAutomationName) => {
                let customGroupingsValue = customGroupings[customGroupingsAutomationName];
                let position;
                for (let i = 0; i < automations.length; i++) {
                    if (automations[i].alias === customGroupingsAutomationName) {
                        position = i;
                        break;
                    }
                }

                automations[position] = this.overWriteFirstEntityId(automations[position], customGroupingsValue, "trigger", "platform");
                automations[position] = this.overWriteFirstEntityId(automations[position], customGroupingsValue, "condition", "condition");
                automations[position] = this.overWriteFirstEntityId(automations[position], customGroupingsValue, "action", "service");
            }
        );

        this.setState({
            addGroups: undefined,
            customGroupings: customGroupings,
            automations: automations
        });
    }

    mergeGroupingsAndAutomations() {
        let automations = this.state.automations;
        let customGroupings = this.state.customGroupings;
        if (automations === undefined || customGroupings === undefined) {
            return;
        }

        let customGroupingsKeys = Object.keys(customGroupings);
        automations = automations.filter(
            (automation) => {
                return !this.doesThisHaveABase(automation, customGroupingsKeys);

            }
        );

        this.setState({
            hasMergedGroupingsAndAutomations: true,
            automations: automations
        });


    }

    splitBasedOnCustomGroupings(automations, customGroupings) {
        let newAutomations = [];
        automations.forEach(
            (automation) => {
                newAutomations.push(automation);
                let comparableCustomGrouping = customGroupings[automation.alias];
                if (comparableCustomGrouping) {
                    let triggers = comparableCustomGrouping.trigger;
                    let conditions = comparableCustomGrouping.condition;
                    let actions = comparableCustomGrouping.action;
                    if (triggers.length + conditions.length + actions.length > 0) {
                        // the following solution only works if offices have a standard naming convention. Otherwise, too many automations will be created.
                        // It assumes that if no entity_id has been written for an office, it should use the original

                        let amountOfEntityIds = Math.max(triggers.length > 0 ? Object.keys(triggers[0].entity_ids).length : 0,
                            conditions.length > 0 ? Object.keys(conditions[0].entity_ids).length : 0,
                            actions.length > 0 ? Object.keys(actions[0].entity_ids).length : 0);

                        for (let i = 1; i < amountOfEntityIds; i++) {
                            let newAutomation = JSON.parse(JSON.stringify(automation));
                            let currentName = this.calculateCurrentName(i);
                            newAutomation = this.replaceEntityIds(newAutomation, comparableCustomGrouping, currentName);
                            newAutomation.alias = automation.alias + "_" + currentName;
                            newAutomations.push(newAutomation);
                        }
                    }
                }
            }
        );

        return newAutomations;
    }

    replaceEntityIds(newAutomation, comparableCustomGrouping, currentName) {
        let newActions = newAutomation.action;
        comparableCustomGrouping.action.forEach(
            (action) => {
                let index = newActions.findIndex(
                    (element) => {
                        return element.service === action.label;
                    }
                );
                if (index > -1) {
                    newActions[index].data.entity_id = action.entity_ids[currentName];
                }
            }
        );

        let triggerAndConditionApproach = (type, platform) => {
            let newOfType = newAutomation[type];
            comparableCustomGrouping[type].forEach(
                (ofType) => {
                    let index = newOfType.findIndex(
                        (element) => {
                            return element[platform] === ofType.label;
                        }
                    );
                    if (index > -1) {
                        newOfType[index].entity_id = ofType.entity_ids[currentName];
                    }
                });
        };

        triggerAndConditionApproach("trigger", "platform");
        triggerAndConditionApproach("condition", "condition");
        return newAutomation;
    }

    doesThisHaveABase(automation, customGroupingsKeys) {
        let alias = automation.alias;
        let foundBase = false;
        customGroupingsKeys.forEach(
            (key) => {
                let split = alias.split(key);
                if (split.length > 1 && split[1].charAt(0) === "_") {
                    foundBase = true;
                }
            }
        );
        return foundBase;
    }

    calculateCurrentName(i) {
        if (i === 0) {
            return "original_office";
        } else {
            return "Room" + i;
        }
    }

    deleteAutomation(automationNumber) {
        let automations = this.state.automations;
        let automationName = "";
        automations = automations.filter(
            (automation, i) => {
                if (i === automationNumber) {
                    automationName = automation.alias;
                }
                return i !== automationNumber;
            }
        );
        let customGroupings = this.state.customGroupings;
        delete customGroupings[automationName];
        this.setState({
            automations: automations,
            customGroupings: customGroupings
        });
    }

    removeGroup(automationName, officeName) {
        let customGroupings = this.state.customGroupings;
        customGroupings[automationName] = this.removeOffice(customGroupings[automationName], officeName);
        this.setState({
            customGroupings: customGroupings
        })
    }

    removeOffice(customGrouping, officeName) {
        let algo = (type) => {
            customGrouping[type].forEach(
                (ofType) => {
                    delete ofType.entity_ids[officeName];

                }
            );
        };
        algo("trigger");
        algo("condition");
        algo("action");

        return customGrouping;
    }

    deleteComponent(type, position, automationNumber) {
        let platform = type === "trigger" ? "platform" : type === "condition" ? "condition" : "service";

        let automations = this.state.automations;
        let savedOfType;
        automations[automationNumber][type] = automations[automationNumber][type].filter(
            (ofType, i) => {
                savedOfType = ofType;
                return i !== position;
            }
        )
        let customGroupings = this.state.customGroupings;
        customGroupings[automations[automationNumber].alias][type] = customGroupings[automations[automationNumber].alias][type].filter(
            (component) => {
                return component.label !== savedOfType[platform]
            }
        )
        this.setState({
            automations: automations,
            customGroupings: customGroupings
        })
    }
}

export default App;