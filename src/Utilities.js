export function addNewRoomToGrouping(addGroups, key, position) {
    let officeName;
    if(Object.keys(addGroups[key][position].entity_ids).length === 0) {
        officeName = "original_office";
    } else {
        officeName = "Room" + Object.keys(addGroups[key][position].entity_ids).length;
    }

    let addNewOffice = (key) => {
        addGroups[key].forEach(
            (component, position) => {
                addGroups[key][position].entity_ids[officeName] = "";
            }
        );
    };

    addNewOffice("trigger");
    addNewOffice("condition");
    addNewOffice("action");
    return addGroups;
}