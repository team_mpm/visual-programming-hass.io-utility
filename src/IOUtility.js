import $ from "jquery";

export function loadConfiguration(callBack) {
    let url = 'http://raspberrypi.local:5000/load';

    let request = new XMLHttpRequest();
    request.timeout = 60000;
    request.open('GET', url, true);
    request.send(null);
    request.onreadystatechange = function () {
        let result = '{}';
        let type = request.getResponseHeader('Content-Type');

        if (request.readyState === 4 && request.status === 200 && type.indexOf("text") !== 1) {
            result = request.responseText;
        }
        let jsonResult = $.parseJSON(result);
        callBack(jsonResult);
    };
}

export function loadServices(callBack) {
    let url = 'http://raspberrypi.local:8123/api/services';

    let request = new XMLHttpRequest();
    request.timeout = 60000;
    request.open('GET', url, true);
    request.send(null);
    request.onreadystatechange = function () {
        let result = '[]';
        let type = request.getResponseHeader('Content-Type');

        if (request.readyState === 4 && request.status === 200 && type.indexOf("text") !== 1) {
            result = request.responseText;
        }
        let jsonResult = $.parseJSON(result);
        callBack(jsonResult);
    };
}

export function storeUnusedParametersSeparate(configuration) {
    let innerConfiguration = configuration;
    let returnConfiguration = {};
    let automation;
    for (let key in innerConfiguration) {
        if (innerConfiguration.hasOwnProperty(key)) {
            if (key === "automation") {
                automation = innerConfiguration[key];
                returnConfiguration[key] = {"link": "automations.yaml"};
            }
            else {
                returnConfiguration[key] = innerConfiguration[key];
            }
        }
    }
    return [returnConfiguration, automation];
}

export function loadStateApi(callback) {
    let url = 'http://raspberrypi.local:8123/api/states';

    let request = new XMLHttpRequest();
    request.timeout = 60000;
    request.open('GET', url, true);
    request.send(null);
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            let type = request.getResponseHeader('Content-Type');
            if (type.indexOf("text") !== 1) {
                let result = request.responseText;
                let jsonResult = JSON.parse(result);
                callback(jsonResult);
            }
        }
    };
}

export function sendToCompile(asJson, customGroupings) {

    let url = 'http://raspberrypi.local:5000/save';
    let request = new XMLHttpRequest();
    request.timeout = 60000;
    request.open('POST', url, true);
    request.send(asJson);
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            let popupResponse = "";

            if (request.status === 200) {
                popupResponse = "Data was successfully transferred to the transpiler.";
            } else if (request.status === 400) {
                popupResponse = request.responseText.split('"text": "')[1];
            }
            if (popupResponse !== "" && popupResponse !== undefined) {
                saveCustomGroupings(customGroupings);
                console.log(popupResponse);
                alert(popupResponse);


            }
        }

    };
}

export function filterNonComponents(stateApi) {
    return stateApi.filter(
        (element) => {
            return !(element.entity_id.startsWith("automation.")
                || element.entity_id.startsWith("script.")
                || element.entity_id.startsWith("group.")
                || element.entity_id === "persistent_notification.notification"
                || element.entity_id === "persistent_notification.config_entry_discovery"
                || element.entity_id.startsWith("updater."));


        }
    );
}

export function findFriendlyName(entity_id, stateApi) {
    let result;
    stateApi.forEach(
        (element) => {
            if (element.entity_id === entity_id) {
                result = element.attributes.friendly_name;
            }
        }
    );
    return result;
}

export function getServicesAsList(input) {
    let serviceList = [];
    input.forEach(
        (service) => {
            for (let key in service.services) {
                if (service.services.hasOwnProperty(key)) {
                    let obj = {name: service.domain + "." + key, value: service.services[key]};
                    serviceList.push(obj);
                }
            }
        });
    return serviceList;
}

function saveCustomGroupings(customGroupings) {
    let asJson = JSON.stringify(customGroupings);
    let url = 'http://raspberrypi.local:5000/custom_groupings';
    let request = new XMLHttpRequest();
    request.timeout = 60000;
    request.open('PUT', url, true);
    request.send(asJson);
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            let popupResponse = "";

            if (request.status === 200) {
                popupResponse = "Custom groupings has been saved.";
            } else if (request.status === 400) {
                popupResponse = request.responseText.split('"text": "')[1];
            }
            if (popupResponse !== "" && popupResponse !== undefined) {
                console.log(popupResponse);
            }
        }

    };
}


export function loadCustomGroupings(callback) {
    let url = 'http://raspberrypi.local:5000/custom_groupings';

    let request = new XMLHttpRequest();
    request.timeout = 60000;
    request.open('GET', url, true);
    request.send(null);
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            let type = request.getResponseHeader('Content-Type');
            if (type.indexOf("text") !== 1) {
                let result = request.responseText;
                let jsonResult = JSON.parse(result);
                callback(jsonResult);
            }
        }
    };
}