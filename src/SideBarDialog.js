import React, {Component} from "react";
import Dialog from 'material-ui/Dialog';
import {DropDownMenu, FlatButton, MenuItem} from "material-ui";
import {findFriendlyName} from "./IOUtility";
import TriggerState from "./components/automations/triggers/TriggerState";
import ConditionNumericState from "./components/automations/conditions/ConditionNumericState";
import ActionCallService from "./components/automations/actions/ActionCallService";
import TriggerTime from "./components/automations/triggers/TriggerTime";
import ConditionTime from "./components/automations/conditions/ConditionTime";

class SideBarDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newComponent: undefined
        };
    }

    render() {
        let actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.props.cancel}
            />,
            <FlatButton
                label="Save"
                primary={true}
                onClick={() => this.props.verifyAndSave(this.state.newComponent)}
            />
        ];

        return <Dialog
            title={"Specify type " + (this.props.specifyNewComponent !== undefined ? this.props.specifyNewComponent.type : "")}
            actions={actions}
            open={this.props.specifyNewComponent !== undefined}>
            {this.props.specifyNewComponent !== undefined && this.props.specifyNewComponent.type === "trigger" ?
                <DropDownMenu value={this.props.specifyNewComponent.value}
                              onChange={this.props.handleChange.bind(this)}>
                    <MenuItem value={1} primaryText="Numeric State Trigger" />
                    <MenuItem value={2} primaryText="State Trigger" />
                    <MenuItem value={3} primaryText="MQTT Trigger" />
                    <MenuItem value={4} primaryText="Template Trigger" />
                    <MenuItem value={5} primaryText="Time Trigger" />
                    <MenuItem value={6} primaryText="Sun Trigger" />
                    <MenuItem value={7} primaryText="Zone Trigger" />
                </DropDownMenu> :
                this.props.specifyNewComponent !== undefined && this.props.specifyNewComponent.type === "condition" ?
                    <DropDownMenu value={this.props.specifyNewComponent.value}
                                  onChange={this.props.handleChange.bind(this)}>
                        <MenuItem value={1} primaryText="Numeric State Condition" />
                        <MenuItem value={2} primaryText="State Condition" />
                        <MenuItem value={3} primaryText="Sun Condition" />
                        <MenuItem value={4} primaryText="Template Condition" />
                        <MenuItem value={5} primaryText="Time Condition" />
                        <MenuItem value={6} primaryText="Zone Condition" />
                    </DropDownMenu> : this.props.specifyNewComponent !== undefined && this.props.specifyNewComponent.type === "action" ?
                    <DropDownMenu value={this.props.specifyNewComponent.value}
                                  onChange={this.props.handleChange.bind(this)}>
                        <MenuItem value={1} primaryText="Call Service" />
                        <MenuItem value={2} primaryText="Condition" />
                        <MenuItem value={3} primaryText="Delay" />
                        <MenuItem value={4} primaryText="Fire Event" />
                        <MenuItem value={5} primaryText="Wait" />
                    </DropDownMenu> : undefined
            }
            {this.props.specifyNewComponent !== undefined && this.props.specifyNewComponent.type === "trigger" ? (
                    this.props.specifyNewComponent.value === 2 ?
                        <TriggerState entity_id={this.props.specifyNewComponent !== undefined ?
                            this.props.specifyNewComponent.entity_id : undefined}
                                      friendly_name={findFriendlyName(this.props.specifyNewComponent.entity_id, this.props.stateApi)}
                                      storeInParent={(object) => {
                                          this.setState({newComponent: object});
                                      }} /> : this.props.specifyNewComponent.value === 5 ?
                        <TriggerTime at={this.props.specifyNewComponent.at} storeInParent={(object) => {
                            this.setState({newComponent: object});
                        }} /> : undefined) :
                this.props.specifyNewComponent !== undefined && this.props.specifyNewComponent.type === "condition" ? (
                        this.props.specifyNewComponent.value === 5 ?
                            <ConditionTime at={this.props.specifyNewComponent.at} storeInParent={(object) => {
                                this.setState({newComponent: object});
                            }} />
                            : <ConditionNumericState entity_id={this.props.specifyNewComponent !== undefined ?
                            this.props.specifyNewComponent.entity_id : undefined}
                                                     friendly_name={findFriendlyName(this.props.specifyNewComponent.entity_id, this.props.stateApi)}
                                                     storeInParent={(object) => {
                                                         this.setState({newComponent: object});
                                                     }}
                            />) :
                    this.props.specifyNewComponent !== undefined && this.props.specifyNewComponent.type === "action" ? (
                        this.props.specifyNewComponent.value === 1 ?
                            <ActionCallService service={""}
                                               services={this.props.services}
                                               data={{}}
                                               stateApi={this.props.stateApi}
                                               storeInParent={(object) => {
                                                   this.setState({newComponent: object});
                                               }}
                            /> :
                            undefined
                    ) : undefined}
        </Dialog>;
    }
}

export default SideBarDialog;