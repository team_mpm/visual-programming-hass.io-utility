import React, {Component} from "react";
import {DragSource} from "react-dnd";
import {ListItem} from "material-ui";

const buttonSource = {
    beginDrag(props) {
        return {
            entity_id: props.entity_id,
            addItem: props.addItem
        };
    },

    endDrag(props, monitor) {
        const item = monitor.getItem();
        const dropResult = monitor.getDropResult();

        if (dropResult) {
            item.addItem(item.entity_id, dropResult.type, dropResult.automationNumber);
        }
    }
};

class DraggableFlatButton extends Component {

    componentDidMount() {
        const img = new Image();
        img.src = 'https://image.flaticon.com/icons/png/128/142/142287.png';
        img.onload = () => {
            this.props.connectDragPreview(img);
        }
    }

    render() {
        return this.props.connectDragSource(<div style={{opacity: this.props.isDragging ? 0.4 : 1}}><ListItem
            primaryText={this.props.label} /></div>);
    }
}

export default DragSource("DragDropItem", buttonSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
}))(DraggableFlatButton);