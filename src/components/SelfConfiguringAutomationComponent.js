import React, {Component} from "react";
import TriggerState from "./automations/triggers/TriggerState";
import ActionCallService from "./automations/actions/ActionCallService";
import "./SelfConfiguringAutomationComponent.css";
import ConditionNumericState from "./automations/conditions/ConditionNumericState";
import ConditionDropZone from "./automations/conditions/ConditionDropZone";
import AddActionZone from "./automations/actions/AddActionZone";
import TriggerDropZone from "./automations/triggers/TriggerDropZone";
import {findFriendlyName} from "../IOUtility";
import TriggerTime from "./automations/triggers/TriggerTime";
import ConditionTime from "./automations/conditions/ConditionTime";

class SelfConfiguringAutomationComponent extends Component {
    render() {
        return <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center'}}>
            <div className={"triggers"}>{this.calculateTriggers()}</div>
            <div className={"conditions"}>{this.calculateConditions()}</div>
            <div className={"actions"}>{this.calculateActions()}</div>
        </div>;
    }

    calculateTriggers() {
        let automation = this.props.automation;
        let triggers = [];
        automation.trigger.forEach((trigger, i) => {
            let platform = trigger.platform;
            let friendlyName = findFriendlyName(trigger.entity_id, this.props.stateApi);

            switch (platform) {
                case "state":
                    triggers.push(<TriggerState key={"trigger-" + i}
                                                friendly_name={friendlyName}
                                                entity_id={trigger.entity_id}
                                                storeInParent={(object) => {
                                                    this.props.storeInParent(object, "trigger", this.props.automationNumber, i);
                                                }}
                                                saveCustomGrouping={this.props.saveCustomGrouping}
                                                allValidComponents={this.props.allValidComponents}
                                                from={trigger.from}
                                                delete={() => this.props.delete("trigger", i)}
                                                customGroupings={this.props.customGroupings}
                                                customGroupingsHasCurrentAutomation={this.customGroupingsHasCurrentAutomation.bind(this)}
                                                to={trigger.to} />);
                    break;
                case "time":
                    triggers.push(<TriggerTime key={"trigger-" + i}
                                               storeInParent={(object) => {
                                                   this.props.storeInParent(object, "trigger", this.props.automationNumber, i);
                                               }}
                                               saveCustomGrouping={this.props.saveCustomGrouping}
                                               allValidComponents={this.props.allValidComponents}
                                               delete={() => this.props.delete("trigger", i)}
                                               customGroupings={this.props.customGroupings}
                                               customGroupingsHasCurrentAutomation={this.customGroupingsHasCurrentAutomation.bind(this)}
                                               at={trigger.at} />);
                    break;
                default:
                    break;
            }
        });
        return <div className={"column"}>
            <div className={"column-title"}>If either of these happens</div>
            {triggers}
            <TriggerDropZone automationNumber={this.props.automationNumber} type={"trigger"} />
        </div>;


    }

    calculateActions() {
        let automation = this.props.automation;
        let actions = [];
        automation.action.forEach((action, i) => {
            if (action.service !== undefined) {
                actions.push(<ActionCallService key={"action-" + i}
                                                service={action.service}
                                                services={this.props.services}
                                                delete={() => this.props.delete("action", i)}
                                                stateApi={this.props.stateApi}
                                                saveCustomGrouping={this.props.saveCustomGrouping}
                                                allValidComponents={this.props.allValidComponents}
                                                customGroupings={this.props.customGroupings}
                                                customGroupingsHasCurrentAutomation={this.customGroupingsHasCurrentAutomation.bind(this)}
                                                storeInParent={(object) => {
                                                    this.props.storeInParent(object, "action", this.props.automationNumber, i);
                                                }}
                                                position={i}
                                                data={action.data ? action.data : {}} />);
            }
        });
        return <div className={"column"}>
            <div className={"column-title"}>Then do this</div>
            {actions}
            <AddActionZone onClick={() => {
                this.props.addItem(
                    {"service": "", "data": {}, "platform": "service"},
                    "action",
                    this.props.automationNumber);
            }} />
        </div>;
    }

    calculateConditions() {
        let automation = this.props.automation;
        let conditions = [];
        automation.condition.forEach((condition, i) => {
            let friendlyName = findFriendlyName(condition.entity_id, this.props.stateApi);
            let conditionName = condition.condition;

            switch (conditionName) {
                case "numeric_state":
                    conditions.push(<ConditionNumericState key={"condition-" + i}
                                                           entity_id={condition.entity_id}
                                                           storeInParent={(object) => {
                                                               this.props.storeInParent(object, "condition", this.props.automationNumber, i);
                                                           }}
                                                           delete={() => this.props.delete("condition", i)}
                                                           friendly_name={friendlyName}
                                                           saveCustomGrouping={this.props.saveCustomGrouping}
                                                           allValidComponents={this.props.allValidComponents}
                                                           customGroupings={this.props.customGroupings}
                                                           customGroupingsHasCurrentAutomation={this.customGroupingsHasCurrentAutomation.bind(this)}
                                                           above={condition.above}
                                                           below={condition.below} />);
                    break;
                case "time":
                    conditions.push(<ConditionTime key={"condition-" + i}
                                                   before={condition.before}
                                                   after={condition.after}
                                                   delete={() => this.props.delete("condition", i)}
                                                   saveCustomGrouping={this.props.saveCustomGrouping}
                                                   allValidComponents={this.props.allValidComponents}
                                                   customGroupings={this.props.customGroupings}
                                                   customGroupingsHasCurrentAutomation={this.customGroupingsHasCurrentAutomation.bind(this)}
                                                   storeInParent={(object) => {
                                                       this.props.storeInParent(object, "condition", this.props.automationNumber, i);
                                                   }}
                                                   weekday={condition.weekday}
                    />);
                    break;
                default:
                    break;
            }
        });


        if (conditions.length === 0) {
            conditions.push(<h3 key={"no-conditions"}>There is currently no conditions</h3>);
        }

        return <div className={"column"}>
            <div className={"column-title"}>While this holds</div>
            {conditions}
            <ConditionDropZone automationNumber={this.props.automationNumber} type={"condition"} />
        </div>;
    }

    customGroupingsHasCurrentAutomation() {
        return this.props.customGroupings[this.props.automation.alias] !== undefined ?
            this.props.automation.alias :
            undefined;

    }
}

export default SelfConfiguringAutomationComponent;
