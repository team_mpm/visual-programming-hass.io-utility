import React, {Component} from "react";
import Dialog from 'material-ui/Dialog';
import {DropDownMenu, FlatButton, MenuItem, TextField} from "material-ui";
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {findFriendlyName} from "../../IOUtility";
import Grouping from "./Grouping";
import {addNewRoomToGrouping} from "../../Utilities";

class HomeAssistantDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            showAllEntityIds: undefined
        };
    }

    render() {
        let actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.props.cancel}
            />,<FlatButton
                label="Delete"
                primary={true}
                onClick={this.props.delete}
            />,
            <FlatButton
                label="Save"
                primary={true}
                onClick={() => this.props.save(this.state.data)}
            />
        ];

        let title = this.props.title;
        if (this.props.isActionCallService) {
            title = "Call service - ";
            let additionalTitle = findFriendlyName(this.state.data[0].value, this.props.stateApi);
            if (!additionalTitle) {
                additionalTitle = this.state.data[0].value;
            }
            title += additionalTitle;
        }

        let linkComponent = this.calculateLinkComponent();

        return <div><Dialog
            title={title}
            bodyStyle={{display: 'flex', flexDirection: 'column', alignItems: 'flex-start'}}
            open={this.props.open}
            actions={actions}
            onRequestClose={this.props.cancel}>
            {linkComponent}
            {this.props.isActionCallService ?
                <TextField key={"dialog-dataPoint-key-service"} floatingLabelText={this.state.data[0].key}
                           defaultValue={this.state.data[0].value}
                           onChange={(event, newValue) => this.onChange(0, newValue, false)} />
                : undefined}
            {this.state.data.map(
                (dataPoint, i) => {
                    if (dataPoint.choices) {
                        let handleChange = (value, key) => {
                            let data = this.state.data;
                            let matchingChoice;

                            data.forEach((
                                dataPoint => {
                                    if (dataPoint.key === key) {
                                        matchingChoice = dataPoint;
                                    }
                                }
                            ));
                            matchingChoice.value = value;
                            this.setState({data: data});
                        };
                        return <div key={dataPoint.key + ": " + i}>
                            {dataPoint.key}
                            <DropDownMenu value={dataPoint.value}
                                          onChange={(event, index, value) => handleChange(value, dataPoint.key)}>
                                {dataPoint.choices.map((
                                    (choice, i) => {
                                        return <MenuItem key={"menu-" + i} value={i} primaryText={choice} />;

                                    }
                                ))}
                            </DropDownMenu>
                        </div>;
                    }

                    let valueComponent;
                    if (dataPoint.key === "entity_id" && this.props.customGroupingsHasCurrentAutomation && this.props.customGroupingsHasCurrentAutomation()) { // todo den kommer også herind selvom der ikke er customgroupings
                        let automationName = this.props.customGroupingsHasCurrentAutomation();
                        let matchingCustomGrouping = this.props.customGroupings[automationName].trigger.find(
                            (currentTrigger) => {
                                return Object.keys(currentTrigger.entity_ids).find(
                                    (key) => {
                                        return key === "original_office" && currentTrigger.entity_ids[key] === dataPoint.value;
                                    }
                                );
                            }
                        );
                        valueComponent = <FlatButton onClick={() => {
                            this.setState({
                                showAllEntityIds: this.props.customGroupings[automationName]
                            });
                        }} label={matchingCustomGrouping ? matchingCustomGrouping.entity_ids.original_office : dataPoint.value} />;
                    } else {
                        valueComponent = <TextField floatingLabelText={"Value"}
                                                    defaultValue={dataPoint.value}
                                                    onChange={(event, newValue) => this.onChange(i, newValue, false)} />;
                    }
                    return <div key={"dialog-dataPoint-div" + i} className={"key-value-tuple"}>
                        <TextField floatingLabelText={"Key"}
                                   disabled={true}
                                   defaultValue={dataPoint.key}
                                   onChange={(event, newValue) => this.onChange(i, newValue, true)} />
                        {valueComponent}

                    </div>;
                }
            )}
            {this.props.isActionCallService ?
                <FloatingActionButton onClick={this.addMore.bind(this)}>
                    <ContentAdd />
                </FloatingActionButton> : undefined}
        </Dialog>
            <Grouping addGroups={this.state.showAllEntityIds}
                      addRoom={(key, position) => {
                          let addGroups = this.state.showAllEntityIds;
                          addGroups = addNewRoomToGrouping(addGroups, key, position);
                          this.setState({
                              showAllEntityIds: addGroups
                          })
                      }}
                      allValidComponents={this.props.allValidComponents}
                      customGroupings={this.props.customGroupings}
                      save={(customGroupings) => {
                          this.props.saveCustomGrouping(customGroupings);
                          this.setState({
                            showAllEntityIds: undefined,
                          })
                      }}
                      cancel={() => {
                          this.setState({
                              showAllEntityIds: undefined,
                          })
                      }}

            />
        </div>;
    }

    calculateLinkComponent() {
        let entity_id;
        this.state.data.forEach(
            (element) => {
                if (element.key === "entity_id") {
                    entity_id = element.value;
                }
            }
        );
        if (!entity_id) return undefined;
        return <p>Please see <a target="_blank"
                                href={"https://home-assistant.io/components/" + entity_id.split(".")[0]}>the home
            assistant website</a> for help. </p>;

        // Todo this wont be updated if customGroupings[0] is modified from e.g. sensor.random to light.rgb

    }

    onChange(i, newValue, isKey) {
        let data = this.state.data;
        if (isKey) {
            data[i].key = newValue;
        }
        else {
            data[i].value = newValue;
        }
        this.setState({data: data});
    }

    addMore() {
        let data = this.state.data;
        data.push({key: "", value: ""});
        this.setState({data: data});
    }
}

export default HomeAssistantDialog;