import React, {Component} from "react";
import PropTypes from 'prop-types';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import HomeAssistantDialog from "../HomeAssistantDialog";
import "./ConditionNumericState.css";

class ConditionTime extends Component {

    constructor(props) {
        super(props);
        this.state = {
            before: this.props.before,
            after: this.props.after,
            weekday: this.props.weekday,
            condition: 'time',
            displayDialog: false
        };
    }

    render() {
        return <div className={"condition-numeric-state"}><Card onClick={() => this.setState({displayDialog: true})}>
            <CardHeader className={"card-header"} subtitleStyle={{
                maxWidth: '250px',
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis'
            }}
                        title="Time condition"

            />
            <CardText>
                <div className={"condition-numeric-state-information-holder"}>
                    <div className={"key"}>{"Before"}</div>
                    <div className={"attribute"}>{this.state.before}</div>
                </div>
                <div className={"condition-numeric-state-information-holder"}>
                    <div className={"key"}>{"After"}</div>
                    <div className={"attribute"}>{this.state.after}</div>
                </div>
                <div className={"condition-numeric-state-information-holder"}>
                    <div className={"key"}>{"Weekday"}</div>
                    <div className={"attribute"}>{this.state.weekday !== undefined ? this.getWeekdays()[this.state.weekday] : undefined}</div>
                </div>
            </CardText>
        </Card>
            <HomeAssistantDialog
                data={[
                    {key: "before", value: this.state.before},
                    {key: "after", value: this.state.after},
                    {key: "weekday", value: this.state.weekday ? this.state.weekday : 0, choices:this.getWeekdays()},
                ]}
                open={this.state.displayDialog}
                title={"Time condition"}
                delete={this.props.delete}
                cancel={this.handleClose}
                customGroupings={this.props.customGroupings}
                customGroupingsHasCurrentAutomation={this.props.customGroupingsHasCurrentAutomation}
                componentType={"trigger"}
                saveCustomGrouping={this.props.saveCustomGrouping}
                allValidComponents={this.props.allValidComponents}
                save={this.save} />
        </div>;
    }

    handleClose = () => {
        this.setState({
            displayDialog: false
        });
    };

    save = (newData) => {
        let before = this.state.before;
        let after = this.state.after;
        let weekday = this.state.weekday;
        newData.forEach(
            (dataPoint) => {
                switch (dataPoint.key) {
                    case "before":
                        before = dataPoint.value;
                        break;
                    case "after":
                        after = dataPoint.value;
                        break;
                    case "weekday":
                        weekday = dataPoint.value;
                        break;
                    default:
                        break;
                }
            }
        );

        this.setState({
            displayDialog: false,
            before: before,
            after: after,
            weekday: weekday
        });
        if (this.props.storeInParent) {
            this.props.storeInParent({
                "before": before,
                "after": after,
                "weekday": weekday,
                "condition": this.state.condition

            });
        }
    };

    getWeekdays() {
        return ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
    }
}

ConditionTime.propTypes = {
    before: PropTypes.string,
    after: PropTypes.string,
    weekday: PropTypes.number,
};

export default ConditionTime;