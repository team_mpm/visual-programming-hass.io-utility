import React, {Component} from "react";
import PropTypes from 'prop-types';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import HomeAssistantDialog from "../HomeAssistantDialog";
import "./ConditionNumericState.css";

class ConditionNumericState extends Component {

    constructor(props) {
        super(props);
        this.state = {
            entity_id: this.props.entity_id,
            above: this.props.above,
            below: this.props.below,
            friendly_name: this.props.friendly_name,
            condition: 'numeric_state',
            displayDialog: false
        };
    }

    render() {

        return <div className={"condition-numeric-state"}><Card onClick={() => this.setState({displayDialog: true})}>
            <CardHeader className={"card-header"} subtitleStyle={{maxWidth: '250px', overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis'}}
                title="Condition Numeric state"
                subtitle={this.state.friendly_name}

            />
            <CardText>
                <div className={"condition-numeric-state-information-holder"}>
                    <div className={"key"}>{"Above"}</div>
                    <div className={"attribute"}>{this.state.above}</div>
                </div>
                <div className={"condition-numeric-state-information-holder"}>
                    <div className={"key"}>{"Below"}</div>
                    <div className={"attribute"}>{this.state.below}</div>
                </div>

            </CardText>
        </Card>
            <HomeAssistantDialog
                data={[
                    {key: "entity_id", value: this.state.entity_id},
                    {key: "above", value: this.state.above},
                    {key: "below", value: this.state.below}]}
                open={this.state.displayDialog}
                title={"Condition Numeric State - " + this.state.friendly_name}
                customGroupings={this.props.customGroupings}
                customGroupingsHasCurrentAutomation={this.props.customGroupingsHasCurrentAutomation}
                componentType={"trigger"}
                saveCustomGrouping={this.props.saveCustomGrouping}
                allValidComponents={this.props.allValidComponents}
                delete={this.props.delete}
                cancel={this.handleClose}
                save={this.save} />
        </div>;
    }

    handleClose = () => {
        this.setState({
            displayDialog: false
        });
    };

    save = (newData) => {
        let entity_id = this.state.entity_id;
        let above = this.state.above;
        let below = this.state.below;
        newData.forEach(
            (dataPoint) => {
                switch (dataPoint.key) {
                    case "entity_id":
                        entity_id = dataPoint.value;
                        break;
                    case "above":
                        above = dataPoint.value;
                        break;
                    case "below":
                        below = dataPoint.value;
                        break;
                    default:
                        break;
                }
            }
        );

        this.setState({
            displayDialog: false,
            entity_id: entity_id,
            above: above,
            below: below
        });
        if (this.props.storeInParent) {
            this.props.storeInParent({
                "entity_id": entity_id,
                "above": above,
                "below": below,
                "condition": this.state.condition
            });
        }
    };
}

ConditionNumericState.propTypes = {
    entity_id: PropTypes.string.isRequired,
    above: PropTypes.string,
    below: PropTypes.string
};

export default ConditionNumericState;