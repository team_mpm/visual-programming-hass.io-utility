import React, {Component} from "react";
import PropTypes from 'prop-types';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import "./ActionCallService.css";
import CallServiceDialog from "./CallServiceDialog";

class ActionCallService extends Component {

    constructor(props) {
        super(props);
        this.state = {
            service: this.props.service,
            data: this.props.data,
            choiceFields: {},
            platform: 'service',
            services: this.props.services,
            displayDialog: false
        };
    }

    render() {
        let dataForDialog = {
            key: "Service",
            value: this.getServicePosition(this.state.service),
            choices: this.state.services,
            choiceFields: this.props.data
        };

        return <div className={"action-service"}><Card onClick={() => this.setState({displayDialog: true})}>
            <CardHeader
                subtitleStyle={{maxWidth: '250px', overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis'}}
                title="Call Service"
                subtitle={this.state.service}

            />
            <CardText/>
        </Card>
            <CallServiceDialog data={dataForDialog}
                               open={this.state.displayDialog}
                               customGroupings={this.props.customGroupings}
                               delete={this.props.delete}
                               customGroupingsHasCurrentAutomation={this.props.customGroupingsHasCurrentAutomation}
                               saveCustomGrouping={this.props.saveCustomGrouping}
                               allValidComponents={this.props.allValidComponents}
                               cancel={this.handleClose}
                               position={this.props.position}
                               save={this.save} />
        </div>;
    }

    handleClose = () => {
        this.setState({
            displayDialog: false
        });
    };

    save = (newData) => {
        let newService = newData.choices[newData.value].name;
        let choiceFieldsTemp = newData.choiceFields;
        let choiceFields = {};

        Object.keys(choiceFieldsTemp).forEach(
            (field) => {
                let value = choiceFieldsTemp[field];
                if (value !== "" || field === "entity_id") { // todo entity_ids
                    choiceFields[field] = value;
                }
            }
        );
        this.setState({
            displayDialog: false,
            data: newData,
            service: newService,
            choiceFields: choiceFields
        });
        if (this.props.storeInParent) {
            this.props.storeInParent({
                "service": newService,
                "data": choiceFields
            });
        }
    };

    getServicePosition(givenService) {
        if (!givenService || !this.state.services) {
            return 0;
        }
        let position = 0;
        this.state.services.forEach(
            (service, i) => {
                if (service.name === givenService) {
                    position = i;
                }
            }
        );
        return position;
    }
}

ActionCallService.propTypes = {
    service: PropTypes.string.isRequired,
    data: PropTypes.object.isRequired
};

export default ActionCallService;