import React, {Component} from "react";
import "../../../dnd/DropTarget/DropZone.css";

class AddActionZone extends Component {
    render() {
        return <div className="drop-zone" onClick={this.props.onClick}>Click here to add an action</div>
    }

}

export default AddActionZone;
