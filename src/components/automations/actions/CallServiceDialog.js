import React, {Component} from "react";
import Dialog from 'material-ui/Dialog';
import {DropDownMenu, FlatButton, MenuItem, TextField} from "material-ui";
import Grouping from "../Grouping";
import {addNewRoomToGrouping} from "../../../Utilities";

class CallServiceDialog extends Component {

    constructor(props) {
        super(props);
        let data = this.props.data;
        data.choiceFields = this.buildChoiceFields(data);
        this.state = {
            data: data,
            showAllEntityIds: undefined
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data
        });
    }

    render() {
        let actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.props.cancel}
            />,
            <FlatButton
                label="Delete"
                primary={true}
                onClick={this.props.delete}
            />,
            <FlatButton
                label="Save"
                primary={true}
                onClick={() => {
                    this.props.save(this.state.data);
                }}
            />
        ];
        let title = "Call service";

        let serviceChooser = this.buildServiceChooser();
        let fieldsPresentation = this.buildFieldsPresentation();

        return <div>
            <Dialog
                title={title}
                bodyStyle={{display: 'flex', flexDirection: 'column', alignItems: 'flex-start'}}
                open={this.props.open}
                actions={actions}
                autoScrollBodyContent={true}
                onRequestClose={this.props.cancel}>
                {serviceChooser}
                {fieldsPresentation}
            </Dialog>
            <Grouping addGroups={this.state.showAllEntityIds}
                      addRoom={(key, position) => {
                          let addGroups = this.state.showAllEntityIds;
                          addGroups = addNewRoomToGrouping(addGroups, key, position);
                          this.setState({
                              addGroups: addGroups
                          });
                      }}
                      allValidComponents={this.props.allValidComponents}
                      customGroupings={this.props.customGroupings}
                      save={(customGroupings) => {
                          this.props.saveCustomGrouping(customGroupings);
                          this.setState({
                              showAllEntityIds: undefined
                          });
                      }}
                      cancel={() => {
                          this.setState({
                              showAllEntityIds: undefined
                          });
                      }}

            />
        </div>;
    }

    buildServiceChooser() {
        let choices = this.state.data.choices;

        let handleChange = (value) => {
            let data = this.state.data;
            data.value = value;
            data.choiceFields = this.buildChoiceFields(data);
            this.setState({data: data});
        };

        return <div><DropDownMenu value={this.state.data.value} onChange={(event, index, value) => handleChange(value)}>
            {choices ? choices.map(
                (choice, i) => {
                    return <MenuItem key={"choice-" + i} value={i} primaryText={choice.name} />;
                }
            ) : undefined
            }
        </DropDownMenu>
        </div>;
    }

    buildFieldsPresentation() {
        let fields = {};
        if (this.state.data && this.state.data.choices && this.state.data.choices[this.state.data.value] && this.state.data.choices[this.state.data.value].value) {
            fields = this.state.data.choices[this.state.data.value].value.fields;
        }
        if (Object.keys(fields).length === 0) return undefined;

        let fieldsMapped = Object.keys(fields).map(
            (key, i) => {
                let description = fields[key].description;
                let example = fields[key].example;
                if (example !== undefined) {
                    description += "\nExample: " + example;
                }

                if (!example) {
                    // console.log("todo not implemented", key);
                    // todo not implemented
                    return undefined;
                }

                let value;
                if (key === "entity_id" && this.props.customGroupingsHasCurrentAutomation()) {
                    let automationName = this.props.customGroupingsHasCurrentAutomation();

                    let comparableCustomGroupingsComponent = this.props.customGroupings[automationName].action[this.props.position];
                    let label = comparableCustomGroupingsComponent ? comparableCustomGroupingsComponent.entity_ids["original_office"] : undefined;
                    if (!label) {
                        label = "Please fill entity_id for all rooms";
                    }

                    value = <FlatButton onClick={() => {
                        this.setState({showAllEntityIds: this.props.customGroupings[automationName]});
                    }}
                                        label={label} />;
                } else {
                    value = <TextField id={key + "-textfield-" + i} value={this.state.data.choiceFields[key]}
                                       onChange={(event, newValue) => this.choiceChanged(key, newValue)} />;

                }

                if (typeof example === "number") {
                    return <div key={key + i} style={{display: 'flex', flexDirection: 'column', marginTop: 20}}>
                        <div>{description}</div>
                        <TextField id={key + "-textfield-" + i} value={this.state.data.choiceFields[key]}
                                   onChange={(event, newValue) => this.choiceChanged(key, newValue)} />
                    </div>;
                }
                else if (typeof example === "string") {
                    return <div key={key + i} style={{display: 'flex', flexDirection: 'column', marginTop: 20}}>
                        <div>{description}</div>
                        {value}
                    </div>;
                }
                else if (typeof example === "boolean") {
                    return <div key={key} style={{display: 'flex', flexDirection: 'column'}}>
                        <div>{description}</div>
                        none, true, false,...
                    </div>;
                }
                else {
                    // console.log("not implemented for", typeof example);
                    // todo not implemented
                    return undefined;
                }
            }
        );

        return <div>
            {fieldsMapped}
        </div>;
    }

    buildChoiceFields(data) {
        let choiceFields = {};
        let value = data.value;
        if (data.choices && data.choices[value] && data.choices[value].value && data.choices[value].value.fields) {
            Object.keys(data.choices[value].value.fields).forEach(
                (key) => {
                    choiceFields[key] = "";
                }
            );
        }
        return choiceFields;
    }

    choiceChanged(key, newValue) {
        let data = this.state.data;
        data.choiceFields[key] = newValue;
        this.setState({
            data: data
        });
    }
}

export default CallServiceDialog;