import React, {Component} from "react";
import {AutoComplete, Dialog, FlatButton, FloatingActionButton} from "material-ui";
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

class Grouping extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allIsValid: true
        };
    }

    render() {
        return <Dialog open={this.props.addGroups !== undefined} title={"Add groups for this automation"}
                       actions={[
                           <FlatButton label="Cancel"
                                       primary={true}
                                       onClick={this.props.cancel} />,
                           <FlatButton label="Save"
                                       primary={true}
                                       onClick={() => {
                                           if (this.state.allIsValid) {
                                               let customGroupings = this.props.customGroupings;
                                               customGroupings[this.props.addGroups.automation] = this.props.addGroups;
                                               this.props.save(customGroupings);
                                           }
                                       }} />
                       ]}>
            {this.mapAllFields()}
        </Dialog>;
    }

    mapAllFields() {
        let arr = [];
        if (!this.props.addGroups) {
            return arr;
        }
        let addGroups = this.props.addGroups;
        let allIsValid = true;
        Object.keys(addGroups).forEach(
            (fieldKey) => {
                if (fieldKey !== "automation") {
                    addGroups[fieldKey].forEach(
                        (field, j) => {
                            arr.push(<div key={field.label + "fieldkey:" + fieldKey + "-" + j}
                                          style={{display: 'flex', flexDirection: 'column'}}>
                                <div>{field.label}</div>
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column'
                                    }}>{Object.keys(field.entity_ids).map(
                                    (key, i) => {
                                        let value = field.entity_ids[key];
                                        let matchType = field.entity_ids[Object.keys(field.entity_ids)[0]].split(".")[0];

                                        let validComponents = this.props.allValidComponents.filter(
                                            (component) => {
                                                let currentType = component.entity_id.split(".")[0];
                                                return currentType === matchType;
                                            }
                                        );
                                        let isCurrentlyValid = this.isCurrentlyValid(value, this.props.allValidComponents, matchType);
                                        allIsValid = allIsValid && isCurrentlyValid;
                                        return <div key={fieldKey + "autocomplete" + field.label + i}
                                                    style={{
                                                        display: 'flex',
                                                        flexDirection: 'row',
                                                        alignItems: 'center'
                                                    }}>
                                            <AutoComplete id={fieldKey + "autocomplete" + field.label + i}
                                                          searchText={value}
                                                          dataSource={validComponents}
                                                          errorText={!isCurrentlyValid ? "Type should match the first entity." : undefined}
                                                          dataSourceConfig={{
                                                              text: 'entity_id',
                                                              value: 'entity_id'
                                                          }}
                                                          floatingLabelText={key}
                                                          filter={AutoComplete.fuzzyFilter}
                                                          onUpdateInput={(newValue) => {
                                                              addGroups[fieldKey][j].entity_ids[key] = newValue;

                                                              allIsValid = true;
                                                              Object.keys(addGroups).forEach(
                                                                  (fieldKey) => {
                                                                      if (fieldKey !== 'automation') {
                                                                          addGroups[fieldKey].forEach(
                                                                              (field, j) => {
                                                                                  Object.keys(field.entity_ids).forEach(
                                                                                      (key, i) => {
                                                                                          let thisMatchType = field.entity_ids[Object.keys(field.entity_ids)[0]].split(".")[0];

                                                                                          let value = field.entity_ids[key];
                                                                                          allIsValid = allIsValid && this.isCurrentlyValid(value, this.props.allValidComponents, thisMatchType);
                                                                                      });
                                                                              });
                                                                      }
                                                                  });
                                                              this.setState({
                                                                  allIsValid: allIsValid
                                                              });
                                                          }
                                                          }

                                            />
                                                {key !== "original_office" && <div>
                                                    <FloatingActionButton onClick={() => {
                                                        this.props.removeGroup(this.props.addGroups.automation, key);
                                                    }}>
                                                        <ContentRemove />
                                                    </FloatingActionButton>
                                                </div>}

                                        </div>;
                                    }
                                )}</div>
                                <br />
                                <div>
                                    <FloatingActionButton onClick={() => {
                                        this.props.addRoom(fieldKey, j);
                                    }}>
                                        <ContentAdd />
                                    </FloatingActionButton>
                                </div>
                                <br />
                            </div>);
                        }
                    );
                }
            }
        );
        return arr;
    }

    isCurrentlyValid(value, validComponents, matchType) {
        let currentlyValid = validComponents.find(
            (component) => {
                return value === component.entity_id;
            }
        ) !== undefined && value.split(".")[0] === matchType;

        return currentlyValid;

    }
}

export default Grouping;
