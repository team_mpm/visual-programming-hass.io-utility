import React, {Component} from "react";
import "../../../dnd/DropTarget/DropZone.css";
import {DropTarget} from "react-dnd";

const buttonTarget = {
    drop() {
        return {type: "trigger"};
    }
};

class TriggerDropZone extends Component {
    render() {
        const {canDrop, isOver, connectDropTarget} = this.props;
        const isActive = canDrop && isOver;

        return connectDropTarget(<div className="drop-zone">{isActive ? "Release to drop" : "Drag a component here."}</div>);
    }
}

export default DropTarget("DragDropItem", buttonTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
}))(TriggerDropZone);
