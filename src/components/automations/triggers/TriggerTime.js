import React, {Component} from "react";
import PropTypes from 'prop-types';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import HomeAssistantDialog from "../HomeAssistantDialog";
import "./TriggerState.css";

class TriggerTime extends Component {

    constructor(props) {
        super(props);
        this.state = {
            at: this.props.at,
            platform: 'time',
            displayDialog: false
        };
    }

    render() {

        return <div className={"trigger-state"}><Card onClick={() => this.setState({displayDialog: true})}>
            <CardHeader className={"card-header"} subtitleStyle={{
                maxWidth: '250px',
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis'
            }}
                        title="Trigger Time"
            />
            <CardText>
                <div className={"trigger-state-information-holder"}>
                    <div className={"key"}>{"At"}</div>
                    <div className={"attribute"}>{this.state.at}</div>
                </div>
            </CardText>
        </Card>
            <HomeAssistantDialog
                data={[
                    {key: "at", value: this.state.at}]}
                open={this.state.displayDialog}
                title={"Trigger Time"}
                customGroupings={this.props.customGroupings}
                customGroupingsHasCurrentAutomation={this.props.customGroupingsHasCurrentAutomation}
                componentType={"trigger"}
                saveCustomGrouping={this.props.saveCustomGrouping}
                allValidComponents={this.props.allValidComponents}
                delete={this.props.delete}
                cancel={this.handleClose}
                save={this.save} />
        </div>;
    }

    handleClose = () => {
        this.setState({
            displayDialog: false
        });
    };

    save = (newData) => {
        let at = this.state.at;
        newData.forEach(
            (dataPoint) => {
                switch (dataPoint.key) {
                    case "at":
                        at = dataPoint.value;
                        break;
                    default:
                        break;
                }
            }
        );

        this.setState({
            displayDialog: false,
            at: at
        });
        if (this.props.storeInParent) {
            this.props.storeInParent({
                "at": at,
                "platform": this.state.platform
            });
        }
    };
}

TriggerTime.propTypes = {
    at: PropTypes.string
};

export default TriggerTime;