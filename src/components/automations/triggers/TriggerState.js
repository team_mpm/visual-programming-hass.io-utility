import React, {Component} from "react";
import PropTypes from 'prop-types';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import HomeAssistantDialog from "../HomeAssistantDialog";
import "./TriggerState.css";

class TriggerState extends Component {

    constructor(props) {
        super(props);
        this.state = {
            entity_id: this.props.entity_id,
            friendly_name: this.props.friendly_name,
            from: this.props.from,
            to: this.props.to,
            platform: 'state',
            displayDialog: false
        };
    }

    render() {

        return <div className={"trigger-state"}><Card onClick={() => this.setState({displayDialog: true})}>
            <CardHeader className={"card-header"} subtitleStyle={{
                maxWidth: '250px',
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis'
            }}
                        title="Trigger State"
                        subtitle={this.state.friendly_name}

            />
            <CardText>
                <div className={"trigger-state-information-holder"}>
                    <div className={"key"}>{"From"}</div>
                    <div className={"attribute"}>{this.state.from}</div>
                </div>
                <div className={"trigger-state-information-holder"}>
                    <div className={"key"}>{"To"}</div>
                    <div className={"attribute"}>{this.state.to}</div>
                </div>

            </CardText>
        </Card>
            <HomeAssistantDialog
                data={[
                    {key: "entity_id", value: this.state.entity_id},
                    {key: "from", value: this.state.from},
                    {key: "to", value: this.state.to}]}
                open={this.state.displayDialog}
                title={"Trigger State - " + this.state.friendly_name}
                customGroupings={this.props.customGroupings}
                customGroupingsHasCurrentAutomation={this.props.customGroupingsHasCurrentAutomation}
                componentType={"trigger"}
                saveCustomGrouping={this.props.saveCustomGrouping}
                allValidComponents={this.props.allValidComponents}
                cancel={this.handleClose}
                delete={this.props.delete}
                save={this.save} />
        </div>;
    }

    handleClose = () => {
        this.setState({
            displayDialog: false
        });
    };

    save = (newData) => {
        let entity_id = this.state.entity_id;
        let from = this.state.from;
        let to = this.state.to;
        newData.forEach(
            (dataPoint) => {
                switch (dataPoint.key) {
                    case "entity_id":
                        entity_id = dataPoint.value;
                        break;
                    case "from":
                        from = dataPoint.value;
                        break;
                    case "to":
                        to = dataPoint.value;
                        break;
                    default:
                        break;
                }
            }
        );

        this.setState({
            displayDialog: false,
            entity_id: entity_id,
            from: from,
            to: to
        });
        if (this.props.storeInParent) {
            this.props.storeInParent({
                "entity_id": entity_id,
                "from": from,
                "to": to,
                "platform": this.state.platform
            });
        }
    };
}

TriggerState.propTypes = {
    entity_id: PropTypes.string.isRequired,
    from: PropTypes.string,
    to: PropTypes.string
};

export default TriggerState;